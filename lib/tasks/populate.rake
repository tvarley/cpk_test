require 'progress_bar'

desc 'Populate Base table one'
task populate_base: :environment do
  num_sites = 500
  num_locations = 50
  num_measurements = 100
  bar = ProgressBar.new(num_sites * num_locations * num_measurements)
  (1..num_sites).each do |site_id|
    (1..num_locations).each do |location_id|
      (1..num_measurements).each do |measurement|
        Table1.create!(
          site_id: site_id,
          location_id: (site_id * num_sites) + location_id,
          device_id: (site_id * num_sites) + location_id,
          timestamp: rand(6.years).seconds.ago,
          temperature: 11 + rand((1..20)),
          moisture: 12 + rand((1..10))
        )
        bar.increment!
      end
    end
  end
end
