class CreateTable1s < ActiveRecord::Migration[5.2]
  def change
    create_table :table1s do |t|
      t.integer :site_id, null: false
      t.integer :device_id, null: false
      t.integer :location_id, null: false
      t.datetime :timestamp, null: false
      t.decimal :temperature
      t.decimal :moisture
      t.integer :inA
      t.integer :inB
      t.integer :hiB
      t.integer :inC
      t.integer :hiA
      t.decimal :salinity
      t.integer :rssi
      t.integer :battery
      t.boolean :from_repeater
    end

    # Add index after inserts
    # add_index :table1s, :site_id
    # add_index :table1s, :device_id
    # add_index :table1s, :location_id
    # add_index :table1s, :timestamp
  end
end
